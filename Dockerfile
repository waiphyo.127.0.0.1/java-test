# Use the official OpenJDK base image
FROM openjdk:11-jre-slim

# Set the working directory in the container
WORKDIR /app

# Copy the compiled Java application into the container
COPY HelloWorld.class /app

# Define the command to run the Java application
CMD ["java", "HelloWorld"]

